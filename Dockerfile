FROM wordpress:php7.4-fpm

RUN apt-get update && apt-get install -y locate procps

ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["php-fpm"]